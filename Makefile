# REQUIRED SECTION
ROOT_DIR:=$(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))

CI_PROJECT_NAME?=spring-demo-k8s
MINIKUBE_CPUS?=4
MINIKUBE_MEMORY?=8g
MINIKUBE_NODES?=1

CI_PROJECT_ID?=1

HELM_CHANNEL?=stable

# END OF REQUIRED SECTION

.PHONY: help

start:
	@minikube -p $(CI_PROJECT_NAME) start --addons=ingress,metrics-server --cpus=$(MINIKUBE_CPUS) --memory=$(MINIKUBE_MEMORY) --nodes=$(MINIKUBE_NODES)

dashboard:
	@minikube -p $(CI_PROJECT_NAME) dashboard

kubectl:
	@kubectl config use-context $(CI_PROJECT_NAME)

repoadd:
	@helm --kube-context $(CI_PROJECT_NAME) repo add $(CI_PROJECT_NAME) https://gitlab.com/api/v4/projects/$(CI_PROJECT_ID)/packages/helm/$(HELM_CHANNEL)

regcred: kubectl
	@kubectl create secret generic regcred --from-file=.dockerconfigjson=$(HOME)/.docker/config.json  --type=kubernetes.io/dockerconfigjson

regcred-delete: kubectl
	@kubectl delete secret regcred

install:
	@helm install gradle spring-demo-k8s/spring-demo-k8s --set  imagePullSecrets[0].name=regcred -f src/test/helm/values.integration.yaml

upgrade:
	@helm upgrade gradle spring-demo-k8s/spring-demo-k8s --set  imagePullSecrets[0].name=regcred -f src/test/helm/values.integration.yaml # --set kafka.topics[0].name=tweets --set kafka.topics[1].name=create-tweets

status:
	@./gradlew :helmStatusGradleOnMinikube

uninstall:
	@helm uninstall gradle

stop:
	@minikube -p $(CI_PROJECT_NAME) stop

delete:
	@minikube -p $(CI_PROJECT_NAME) delete

docker-env:
	@eval $(minikube -p $(CI_PROJECT_NAME) docker-env)
docker-env-print:
	@minikube -p $(CI_PROJECT_NAME) docker-env

build-minikube: docker-env
	@./gradlew :bootBuildImage

help: ##@other Show this help.
	@perl -e '$(HELP_FUN)' $(MAKEFILE_LIST)
