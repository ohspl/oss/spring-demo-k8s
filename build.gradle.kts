import com.github.jk1.license.filter.DependencyFilter
import com.github.jk1.license.filter.ExcludeDependenciesWithoutArtifactsFilter
import com.github.jk1.license.filter.LicenseBundleNormalizer
import com.github.jk1.license.filter.SpdxLicenseBundleNormalizer
import com.github.jk1.license.render.InventoryHtmlReportRenderer
import com.github.jk1.license.render.JsonReportRenderer
import com.github.jk1.license.render.ReportRenderer
import com.github.jk1.license.render.TextReportRenderer
import java.math.RoundingMode
import java.util.regex.Pattern
import net.razvan.JacocoToCoberturaTask
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.openrewrite.gradle.RewriteRunTask

plugins {
  id("org.springframework.boot") version "3.3.2"
  id("io.spring.dependency-management") version "1.1.6"
  kotlin("jvm") version "2.1.0"
  kotlin("kapt") version "2.1.0"
  kotlin("plugin.spring") version "2.1.0"
  id("com.diffplug.spotless") version "6.25.0"
  jacoco
  id("net.razvan.jacoco-to-cobertura") version "1.2.0"
  id("com.citi.helm-commands") version "2.2.0"
  id("com.citi.helm") version "2.2.0"
  id("com.citi.helm-publish") version "2.2.0"
  id("com.citi.helm-releases") version "2.2.0"
  id("org.openrewrite.rewrite") version ("6.22.0")
  id("com.github.jk1.dependency-license-report") version "2.9"
  id("org.sonarqube") version "5.1.0.4882"
}

// region Java
java {
  withSourcesJar()
  withJavadocJar()
  sourceCompatibility = JavaVersion.VERSION_21
}

configurations {
  compileOnly {
    // enable annotation processing in compile taks
    extendsFrom(configurations.annotationProcessor.get())
  }
}
// endregion

// region dependencies
repositories {
  mavenCentral()
  mavenLocal()
}

extra["springCloudVersion"] = "2023.0.3"

dependencies {
  implementation("org.springframework.boot:spring-boot-starter-actuator")
  implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
  implementation("org.mapstruct:mapstruct:1.6.0")
  kapt("org.mapstruct:mapstruct-processor:1.6.0")
  implementation("org.jetbrains.kotlin:kotlin-reflect")

  implementation("org.springframework.cloud:spring-cloud-starter")

  implementation("org.flywaydb:flyway-core")
  implementation("org.flywaydb:flyway-database-postgresql")
  runtimeOnly("org.postgresql:postgresql") // flyway does not support R2DBC

  implementation("org.springframework.boot:spring-boot-starter-data-r2dbc")
  runtimeOnly("org.postgresql:r2dbc-postgresql")
  // https://docs.spring.io/spring-boot/docs/current/reference/html/web.html#web.spring-hateoas
  /*
  	@warning
  	spring-boot-starter-hateoas is specific to Spring MVC and should not be combined with Spring WebFlux.
  	In order to use Spring HATEOAS with Spring WebFlux,
  	you can add a direct dependency on org.springframework.hateoas:spring-hateoas along
  	with spring-boot-starter-webflux.
  */
  //  implementation("org.springframework.hateoas:spring-hateoas")
  implementation("org.springframework.boot:spring-boot-starter-webflux")

  // https://docs.spring.io/spring-cloud-kubernetes/reference/kubernetes-awareness.html
  implementation("org.springframework.cloud:spring-cloud-starter-bootstrap")
  implementation("org.springframework.cloud:spring-cloud-starter-kubernetes-fabric8-all")

  // Event dependencies
  implementation("org.springframework.cloud:spring-cloud-stream")
  //  implementation("org.springframework.cloud:spring-cloud-stream-binder-kafka")
  implementation("org.springframework.cloud:spring-cloud-stream-binder-kafka-reactive")

  //	//  dependencies for kafka streams
  //  implementation("org.apache.kafka:kafka-streams")
  //  implementation("org.springframework.cloud:spring-cloud-stream-binder-kafka-streams")

  testImplementation("org.springframework.boot:spring-boot-starter-test")
  testImplementation("com.playtika.testcontainers:embedded-postgresql:3.1.9")
  //  testImplementation("com.playtika.testcontainers:embedded-kafka:3.1.5")
  testImplementation("org.testcontainers:junit-jupiter")

  testImplementation("org.springframework.boot:spring-boot-starter-test")
  testImplementation("org.springframework.cloud:spring-cloud-stream-test-binder")
  testImplementation("org.springframework.kafka:spring-kafka-test")

  developmentOnly("org.springframework.boot:spring-boot-docker-compose")

  // OpenRewrite dependencies
  // https://docs.openrewrite.org/recipes/java/spring/boot3/
  rewrite("org.openrewrite.recipe:rewrite-spring:5.18.0")
  // https://docs.openrewrite.org/recipes/java/security
  rewrite("org.openrewrite.recipe:rewrite-java-security:2.11.3")
  // https://docs.openrewrite.org/recipes/java/logging
  rewrite("org.openrewrite.recipe:rewrite-logging-frameworks:2.12.2")
}

// region OpenRewrite
rewrite {
  // rewrite-spring
  // https://docs.openrewrite.org/recipes/java/spring/boot3/springboot3bestpractices
  activeRecipe("org.openrewrite.java.spring.boot3.SpringBoot3BestPractices")

  // https://docs.openrewrite.org/recipes/java/spring/boot3/addroutetrailingslash
  activeRecipe("org.openrewrite.java.spring.boot3.AddRouteTrailingSlash")
  // https://docs.openrewrite.org/recipes/java/spring/http/replacestringliteralswithhttpheadersconstants
  activeRecipe("org.openrewrite.java.spring.http.ReplaceStringLiteralsWithHttpHeadersConstants")

  // https://docs.openrewrite.org/recipes/java/spring/boot3/upgradespringboot_3_0
  activeRecipe("org.openrewrite.java.spring.boot3.UpgradeSpringBoot_3_0")
  // https://docs.openrewrite.org/recipes/java/spring/boot3/upgradespringboot_3_1
  activeRecipe("org.openrewrite.java.spring.boot3.UpgradeSpringBoot_3_1")
  // https://docs.openrewrite.org/recipes/java/spring/boot3/upgradespringboot_3_2
  activeRecipe("org.openrewrite.java.spring.boot3.UpgradeSpringBoot_3_2")
  // https://docs.openrewrite.org/recipes/java/spring/boot3/upgradespringboot_3_3
  activeRecipe("org.openrewrite.java.spring.boot3.UpgradeSpringBoot_3_3")

  // https://docs.openrewrite.org/recipes/java/spring/boot3/precisebeantype
  activeRecipe("org.openrewrite.java.spring.boot3.PreciseBeanType")
  // https://docs.openrewrite.org/recipes/java/spring/boot3/enablevirtualthreads
  activeRecipe("org.openrewrite.java.spring.boot3.EnableVirtualThreads")

  // https://docs.openrewrite.org/recipes/java/spring/framework/migratewebmvcconfigureradapter
  activeRecipe("org.openrewrite.java.spring.framework.MigrateWebMvcConfigurerAdapter")

  // https://docs.openrewrite.org/recipes/java/spring/separateapplicationyamlbyprofile
  activeRecipe("org.openrewrite.java.spring.SeparateApplicationYamlByProfile")

  // security related recipes
  // https://docs.openrewrite.org/recipes/java/security/securetempfilecreation
  activeRecipe("org.openrewrite.java.security.SecureTempFileCreation")
  // https://docs.openrewrite.org/recipes/java/security/javasecuritybestpractices
  activeRecipe("org.openrewrite.java.security.JavaSecurityBestPractices")
  // https://docs.openrewrite.org/recipes/java/security/secrets/findsecrets
  activeRecipe("org.openrewrite.java.security.secrets.FindSecrets")

  // logging recipes
  // https://docs.openrewrite.org/recipes/java/logging/logback/log4jtologback
  activeRecipe("org.openrewrite.java.logging.logback.Log4jToLogback")

  // custom recipes from rewrite.yml
  activeRecipe("org.gitlab.bullshit.devops.springdemok8s.rewrite.SpringComposeDisabledByDefault")

  // https://docs.openrewrite.org/running-recipes/data-tables
  isExportDatatables = true
}
// endregion

dependencyManagement {
  imports {
    mavenBom(
        "org.springframework.cloud:spring-cloud-dependencies:${property("springCloudVersion")}")
  }
}
// endregion

// region tasks
tasks.withType<KotlinCompile> {
  kotlinOptions {
    freeCompilerArgs += "-Xjsr305=strict"
    jvmTarget = java.sourceCompatibility.toString()
  }
}

tasks.withType<Test> {
  useJUnitPlatform()
  failFast = System.getenv("CI") != "true"
}

tasks.test { finalizedBy(tasks.jacocoTestReport) }

tasks.bootBuildImage {
  publish.set(System.getenv("CI").toBoolean())
  val ociName = System.getenv("CI_REGISTRY_IMAGE") ?: project.name
  val imageVersion = (project.version as String).replace("+", "-")
  val refSlug = (System.getenv("CI_COMMIT_REF_SLUG") ?: "latest")
  imageName.set("$ociName:$imageVersion")
  if (imageVersion != refSlug) {
    tags.set(listOf("$ociName:$refSlug"))
  }
  docker {
    publishRegistry {
      username.set(System.getenv("CI_REGISTRY_USER") ?: "")
      password.set(System.getenv("CI_REGISTRY_PASSWORD") ?: "")
      url.set(System.getenv("CI_REPOSITORY_URL") ?: "")
      email.set(System.getenv("GITLAB_USER_EMAIL") ?: "")
    }
  }
}

// When run in pipeline publish helm chart after bootBuildImage
val helmPublishTask = tasks.getByName("helmPublish")

val bBITask =
    tasks.getByName("bootBuildImage") {
      if (System.getenv("CI").toBoolean()) {
        finalizedBy(helmPublishTask)
      }
    }
// endregion

// region codeformatting
val formatterOff = "@formatter:off"
val formatterOn = "@formatter:on"

configure<com.diffplug.gradle.spotless.SpotlessExtension> {
  kotlin {
    // by default the target is every '.kt' and '.kts` file in the java sourcesets
    ktfmt()
    toggleOffOn(formatterOff, formatterOn)
  }
  kotlinGradle {
    target("*.gradle.kts") // default target for kotlinGradle
    ktfmt()
    toggleOffOn(formatterOff, formatterOn)
  }
  yaml {
    target("src/main/resources/**/*.yaml", "src/main/resources/**/*.yml")
    jackson()
    toggleOffOn(formatterOff, formatterOn)
  }
  json { target("src/**/*.json") }
  sql {
    target("src/**/*.sql")
    dbeaver()
    toggleOffOn(formatterOff, formatterOn)
  }
  freshmark {
    target("*.md") // you have to set the target manually
    //		propertiesFile("gradle.properties")		// loads all the properties in the given file
  }
  format("misc") {
    // define the files to apply `misc` to
    target(".gitattributes", ".gitignore", "gradle.properties")
    trimTrailingWhitespace()
    indentWithTabs() // or spaces. Takes an integer argument if you don't like 4
    endWithNewline()
    toggleOffOn(formatterOff, formatterOn)
  }
}

// Run code formatter after openrewrite rewriteRun task
tasks.withType<RewriteRunTask> { finalizedBy("spotlessApply") }
// endregion

// region buildInfo
springBoot { buildInfo() }
// endregion

// region Helm
helm {
  if (System.getenv("CI").toBoolean()) {
    downloadClient { enabled = true }
  }
  repositories {
    // registers the Bitnami repo under the name "bitnami"
    bitnami()
  }
  // filtering step that will resolve placeholders in certain chart files before the chart is
  // packaged
  filtering {
    values.put(
        "imageRepository",
        System.getenv("CI_REGISTRY_IMAGE") ?: "docker.io/library/${project.name}")
    if (!System.getenv("CI").toBoolean()) {
      // latest tag is set by bootBuildImage task
      values.put("imageTag", "latest")
    } else {
      values.put("imageTag", System.getenv("CI_COMMIT_REF_SLUG") ?: "")
    }
  }
  lint {
    // treat linter warnings as errors (failing the build)
    strict = true
    // don't lint dependent charts
    withSubcharts = false
  }
  charts {
    create("main") {
      sourceDir.set(file("src/main/helm"))
      chartVersion.set(project.version.toString())
      renderings {
        // Configure default rendering (gradle task helmRender) to
        // render helm chart for release gradle with values.integration.yaml
        named("default") {
          releaseName.set("gradle")

          valueFiles.from("src/test/helm/values.integration.yaml")

          // validate manifests against the Kubernetes cluster
          // (helm template --validate)
          validate.set(true)
        }
      }
    }
  }

  publishing {
    repositories {
      gitlab {
        url.set(uri("https://gitlab.com/api/v4"))
        projectId.set(Integer.parseInt(System.getenv("CI_PROJECT_ID") ?: "-1"))
        credentials {
          username.set("gitlab-ci-token")
          password.set(System.getenv("CI_JOB_TOKEN") ?: "")
        }
      }
    }
  }

  releases {

    // Define Helm release (install) with the name `gradle` with values for integration tests.
    create("gradle") {
      from(chart("main"))
      valueFiles.from("src/test/helm/values.integration.yaml")
    }
  }

  releaseTargets {
    create("minikube") {
      test {
        // always dump test logs for this target
        showLogs.set(true)
      }
      val contextName = System.getenv("CI_PROJECT_NAME") ?: "${rootProject.name}"
      kubeContext.set(contextName)
    }
  }
} // endregion

// region jacoco covertura
tasks.jacocoTestReport {
  dependsOn(tasks.test)
  finalizedBy(tasks.findByName("jacocoToCobertura"))
  reports {
    xml.required = true
    html.required = !System.getenv("CI").toBoolean()
    csv.required.set(false)
  }
}

tasks.test { finalizedBy(tasks.findByName("jacocoTestReport")) }

tasks.withType(JacocoToCoberturaTask::class) {
  doLast {
    val pattern = Pattern.compile("\\<coverage line-rate=\\\"(?<lineRate>([0-9]*[.])?[0-9]+)\\\".*")

    var coverage = BigDecimal.valueOf(-1)
    val outputFile =
        file("${layout.buildDirectory.get()}/reports/jacoco/test/cobertura-jacocoTestReport.xml")

    if (outputFile.exists()) {
      coverage =
          outputFile
              .readLines()
              .asSequence()
              .map { pattern.matcher(it) }
              .filter { it.matches() }
              .map { it.group("lineRate") }
              .map { it.toBigDecimal() }
              .map { it.multiply(BigDecimal.valueOf(100L)) }
              .map { it.setScale(1, RoundingMode.CEILING) }
              .first()
    }

    println("Code coverage: $coverage")
  }
} // endregion

// region license
licenseReport {
  // https://github.com/jk1/Gradle-License-Report/tree/master?tab=readme-ov-file#allowed-licenses-file
  // SPDX License Identifiers can be found here https://spdx.org/licenses/
  allowedLicensesFile = File("$projectDir/.gitlab/allowed-licenses.json")
  renderers =
      arrayOf<ReportRenderer>(
          JsonReportRenderer("license-bundle.json"), // json file to easy parse license information
          InventoryHtmlReportRenderer("legal.html"), // fancy grouped by license html file
          TextReportRenderer("legal.txt") // standard simple license html file
          )
  filters =
      arrayOf<DependencyFilter>(
          LicenseBundleNormalizer(),
          ExcludeDependenciesWithoutArtifactsFilter(),
          //          ExcludeTransitiveDependenciesFilter(),
          SpdxLicenseBundleNormalizer())
}

tasks.named("check") { dependsOn("checkLicense") }
// copy license files into static web path
tasks.withType<ProcessResources> {
  dependsOn("generateLicenseReport")
  from(layout.buildDirectory.dir("reports/dependency-license/")) {
    include("legal.txt", "license-bundle.json", "legal.html")
    into(".well-known")
  }
} // endregion

// region SonarCloud
// See: https://docs.sonarcloud.io/advanced-setup/ci-based-analysis/sonarscanner-for-gradle/
sonarqube {
  properties {
    //		property(
    //			"sonar.branch.name",
    //			System.getenv("CI_COMMIT_REF_NAME")
    //		) // To use the property "sonar.branch.name" and analyze branches, Developer Edition or
    // above is required.
    property("sonar.projectKey", "owspl_spring-demo-k8s")
    property("sonar.projectName", project.name)
    property("sonar.organization", "owsploos")

    //		property("sonar.sources", layout.projectDirectory.dir("src/main").asFile.absolutePath)
    //		property(
    //			"sonar.tests",
    //			layout.projectDirectory.dir("src/test").asFile.absolutePath
    //		)
    // only wait in CI
    property("sonar.qualitygate.wait", System.getenv("CI"))
    // iterate through directories
    property(
        "sonar.junit.reportPaths",
        layout.buildDirectory.dir("test-results/test/").get().asFile.absolutePath)
    //		property(
    //			"sonar.coverage.jacoco.xmlReportPaths",
    //			layout.buildDirectory.dir("reports/jacoco/test/")
    //				.get().asFile.absolutePath + "," +
    // layout.buildDirectory.dir("reports/jacoco/functionalTest/")
    //				.get().asFile.absolutePath
    //		)
    property("sonar.sourceEncoding", "UTF-8")
    property("sonar.java.binaries", layout.buildDirectory.dir("classes").get().asFile.absolutePath)
  }
}

// tasks.named("sonar") {
//  dependsOn("check")
//  // you can run sonar with gradle in CI by uncommented the next code line
//  // or with as a CI/CD job with sonar-scanner-cli
//  //  onlyIf { System.getenv("CI").toBoolean() }
// } // endregion SonarCloud
