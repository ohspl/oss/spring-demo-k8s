services:
  postgres:
    image: "postgres:16-alpine@sha256:91e75546b4f229fa92cec21a2bf831ba03a1c101fc197daafff657f153e93a39"
    ports:
    - "127.0.0.1:${CONTAINER_1_EXPOSE_PORT_1:-5432}:5432"
    environment:
      TZ: "Europe/Austria"
      POSTGRES_USER: "${POSTGRES_USER:-postgres}"
      POSTGRES_PASSWORD: "${POSTGRES_PASSWORD:-postgres}"
      POSTGRES_DB: "${POSTGRES_DB:-demo}"
    volumes:
    - "./docker/init.sql:/docker-entrypoint-initdb.d/init.sql"
    healthcheck:
      test: ["CMD-SHELL", "pg_isready"]
      interval: 10s
      timeout: 5s
      retries: 5
  zookeeper:
    image: "confluentinc/cp-zookeeper:7.8.0@sha256:5ca5f3269814804ebf88e4da80f9bdc73df1f70ec66b4b8ed0344159e8d342ec"
    hostname: "zookeeper"
    ports:
    - "2181:2181"
    environment:
      ZOOKEEPER_CLIENT_PORT: 2181
      ZOOKEEPER_TICK_TIME: 2000
    healthcheck:
      test: "echo srvr | nc zookeeper 2181 || exit 1"
      retries: 3
      interval: "10s"
  broker:
    image: "confluentinc/cp-kafka:7.8.0@sha256:adc392d28a1e99e8c9a1ec7f087e9e91041837b35b8b7cc8b8a691b82dd581b0"
    hostname: "broker"
    ports:
    - "9092:9092"
    - "29092:29092"
    - "9999:9999"
    environment:
      KAFKA_BROKER_ID: 1
      KAFKA_ZOOKEEPER_CONNECT: "zookeeper:2181"
      KAFKA_ADVERTISED_LISTENERS: "PLAINTEXT://broker:29092,PLAINTEXT_HOST://localhost:9092"
      KAFKA_LISTENER_SECURITY_PROTOCOL_MAP: "PLAINTEXT:PLAINTEXT,PLAINTEXT_HOST:PLAINTEXT"
      KAFKA_INTER_BROKER_LISTENER_NAME: "PLAINTEXT"
      KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR: 1
      KAFKA_SCHEMA_REGISTRY_URL: "schemaregistry:18085"
      KAFKA_JMX_PORT: 9999
      KAFKA_JMX_HOSTNAME: "${DOCKER_HOST_IP:-127.0.0.1}"
      KAFKA_CONFLUENT_METADATA_SERVER_LISTENERS: "https://0.0.0.0:8091"
      KAFKA_CONFLUENT_METADATA_SERVER_ADVERTISED_LISTENERS: "https://broker:8091"
    depends_on:
      zookeeper:
        condition: "service_healthy"
    healthcheck:
      test: nc -z localhost 9092 || exit -1
      start_period: 15s
      interval: 5s
      timeout: 10s
      retries: 10
  schemaregistry:
    image: "confluentinc/cp-schema-registry:7.8.0@sha256:ae0695b092314be82fe32058c276d99224855090a77ec169423cf82e02d5e3ac"
    hostname: "schemaregistry"
    depends_on:
      broker:
        condition: "service_healthy"
    ports:
    - "8081:8081"
    environment:
      SCHEMA_REGISTRY_KAFKASTORE_BOOTSTRAP_SERVERS: "PLAINTEXT://broker:29092"
      SCHEMA_REGISTRY_KAFKASTORE_CONNECTION_URL: "zookeeper:2181"
      SCHEMA_REGISTRY_HOST_NAME: "schemaregistry"
      SCHEMA_REGISTRY_LISTENERS: "http://0.0.0.0:18085"
    healthcheck:
      start_period: "10s"
      interval: "10s"
      retries: 3
      test: "curl --fail --silent http://schemaregistry:18085/subjects --output /dev/null\
        \ || exit 1"
    labels:
      org.springframework.boot.readiness-check.tcp.disable: true
  webui:
    image: "quay.io/cloudhut/kowl:v1.5.0@sha256:fbf55369af1ed991ec0ce71ab8675a158a181cace6d7f8daac081cede8bade33"
    hostname: "kowl"
#    restart: "on-failure:3"
    ports:
    - "8086:8080"
    environment:
      KAFKA_BROKERS: "broker:29092"
      KAFKA_SCHEMAREGISTRY_ENABLED: "true"
      KAFKA_SCHEMAREGISTRY_URLS: "http://schemaregistry:18085"
    depends_on:
      broker:
        condition: "service_healthy"
      schemaregistry:
        condition: "service_healthy"
    healthcheck:
      start_period: "10s"
      interval: "10s"
      retries: 3
      test: "wget --spider http://localhost:8080 || exit 1"
    labels:
      org.springframework.boot.readiness-check.tcp.disable: true
