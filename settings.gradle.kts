pluginManagement {
  repositories {
    gradlePluginPortal()
    mavenCentral()
  }
}

val name: String by settings

rootProject.name = name

plugins { id("com.mooltiverse.oss.nyx") version "3.0.7" }
// region nyx - releasing
configure<com.mooltiverse.oss.nyx.gradle.NyxExtension> {
  dryRun.set(!System.getenv("CI").toBoolean())
  configurationFile.set(".nyx.yaml")
} // endregion
