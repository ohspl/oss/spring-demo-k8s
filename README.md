# spring-demo-k8s

## Disclamer

Please approach everything with a critical eye. Given the specialized purpose of this service, some decisions may be made hastily.
Feel free to open an issue to initiate discussions about this project.

## Purpose

This demo is used as integration tests and a playground for developing topics within the Kubernetes and Spring ecosystem.

The primary focus is on leveraging a wide array of supporting services to create a complex deployment for thorough testing.


## Overview

```mermaid

graph LR;

subgraph springcloudconfig [Spring Operator]
    operator[Operator]---springdemok8s
    operator---springcloudconfigserver[Spring Cloud Config Server]
end

subgraph springdemok8s [Spring Demo k8s]
    springdemok8s---kafka[Kafka]
    springdemok8s---postgresql[PostgreSQL]
end

````
