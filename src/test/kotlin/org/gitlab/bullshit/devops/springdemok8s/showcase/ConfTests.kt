package org.gitlab.bullshit.devops.springdemok8s.showcase

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.web.reactive.server.WebTestClient

@SpringBootTest
@AutoConfigureWebTestClient
class ConfTests {

  @Autowired private val webTestClient: WebTestClient? = null

  @Test
  fun getShowcaseProperty() {
    val showcase =
        webTestClient!!.get().uri("/conf").exchange().expectBody(String::class.java).returnResult()
    Assertions.assertEquals("hello test!", showcase.responseBody)

    val showcaseField =
        webTestClient
            .get()
            .uri("/conf/field")
            .exchange()
            .expectBody(String::class.java)
            .returnResult()
    Assertions.assertEquals("hello test!", showcaseField.responseBody)
  }
}
