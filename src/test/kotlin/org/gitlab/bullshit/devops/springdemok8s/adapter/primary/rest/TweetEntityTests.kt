package org.gitlab.bullshit.devops.springdemok8s.adapter.primary.rest

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.web.reactive.server.WebTestClient
import reactor.core.publisher.Mono

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebTestClient(timeout = "10000")
class TweetEntityTests {

  @Autowired private val webTestClient: WebTestClient? = null

  @Test
  fun testCreate() {

    val newTweet =
        TweetResource().apply {
          title = "testtweet3"
          message = "testmessage3"
          keywords = "keyword5,keyword6"
          source = "source3"
        }

    val newTweetRepsonse =
        webTestClient!!
            .post()
            .uri("/tweets/")
            .bodyValue(newTweet)
            .exchange()
            .expectStatus()
            .isCreated()
            .expectBody(TweetResource::class.java)
            .returnResult()

    val savedTweet =
        webTestClient
            .get()
            .uri("/tweets/{id}/", newTweetRepsonse.responseBody?.id)
            .exchange()
            .expectStatus()
            .isOk()
            .expectBody(TweetResource::class.java)
            .returnResult()

    Assertions.assertEquals(newTweet.title, savedTweet.responseBody?.title)

    webTestClient
        .get()
        .uri("/tweets/stream/")
        .accept(MediaType.TEXT_EVENT_STREAM)
        .exchange()
        .expectStatus()
        .isOk()
  }

  @Test
  fun testPartialUpdate() {
    val newTweet =
        TweetResource().apply {
          title = "testtweet3"
          message = "testmessage3"
          keywords = "keyword5,keyword6"
          source = "source3"
        }

    val savedTweet =
        webTestClient!!
            .post()
            .uri("/tweets/")
            .bodyValue(newTweet)
            .exchange()
            .expectStatus()
            .isCreated()
            .expectBody(TweetResource::class.java)
            .returnResult()
    Assertions.assertEquals("testtweet3", savedTweet.responseBody!!.title)

    val patch = mapOf("title" to "oskar_title")
    webTestClient
        .patch()
        .uri("/tweets/{id}/", savedTweet.responseBody!!.id)
        .contentType(MediaType.APPLICATION_JSON)
        //    .body(Mono.just<Any>(updateTweet), String.javaClass)
        .body(Mono.just<Any>(patch), String.javaClass)
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody()
        .returnResult()

    val patchedTweet =
        webTestClient
            .get()
            .uri("/tweets/{id}/", savedTweet.responseBody!!.id)
            .exchange()
            .expectStatus()
            .isOk()
            .expectBody(TweetResource::class.java)
            .returnResult()

    Assertions.assertNotNull(patchedTweet.responseBody)
    Assertions.assertEquals("oskar_title", patchedTweet.responseBody!!.title)
  }
}
