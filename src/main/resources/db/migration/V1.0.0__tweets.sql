CREATE
    TABLE
        tweet(
            id BIGSERIAL PRIMARY KEY,
            created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,
            modified_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,
            title VARCHAR(255),
            message text,
            keywords text,
            SOURCE VARCHAR(255)
        );

CREATE
    INDEX tweet_created_at_brin_idx ON
    tweet
        USING brin(created_at);
