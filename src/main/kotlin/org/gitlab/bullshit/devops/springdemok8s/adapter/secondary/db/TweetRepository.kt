package org.gitlab.bullshit.devops.springdemok8s.adapter.secondary.db

import org.springframework.data.repository.reactive.ReactiveCrudRepository
import org.springframework.stereotype.Repository

@Repository internal interface TweetRepository : ReactiveCrudRepository<TweetEntity, Long> {}
