package org.gitlab.bullshit.devops.springdemok8s.adapter.primary.event

import org.gitlab.bullshit.devops.springdemok8s.domain.Tweet
import org.mapstruct.BeanMapping
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.MappingConstants
import org.mapstruct.NullValuePropertyMappingStrategy

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
interface TweetEventMapper {

  @Mapping(target = "id", ignore = true)
  @Mapping(target = "modified_at", expression = "java(java.time.Instant.now())")
  @Mapping(target = "created_at", ignore = true)
  @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
  fun toTweet(createTweetCommand: CreateTweetCommand): Tweet
}
