package org.gitlab.bullshit.devops.springdemok8s.domain

import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

interface Tweets {
  fun findAll(): Flux<Tweet>

  fun findById(id: Long): Mono<Tweet>

  fun save(tweet: Tweet): Mono<Tweet>
}
