package org.gitlab.bullshit.devops.springdemok8s.adapter.primary.event

import java.util.function.Supplier
import java.util.logging.Level
import java.util.logging.Logger
import org.gitlab.bullshit.devops.springdemok8s.domain.Tweet
import org.gitlab.bullshit.devops.springdemok8s.domain.Tweets
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import reactor.core.publisher.Flux

@Configuration
internal class EventConfiguration {
  private val logger = Logger.getLogger(javaClass.name)

  // Avro Model would be a clean solutions.
  // this is just a demo to test spring-operator
  // Supplier are polled once a second by spring
  @Bean
  fun createTweetCommand(): Supplier<CreateTweetCommand> {
    logger.info("initialize suppler")
    return Supplier<CreateTweetCommand> {
      logger.info("send new tweet")
      // @TODO random characters
      CreateTweetCommand("asdf", "asdf", "asdf", "source")
    }
  }

  // outbox
  @Bean
  fun saveTweet(
      tweetRepository: Tweets,
      tweetEventMapper: TweetEventMapper
  ): java.util.function.Function<Flux<CreateTweetCommand>, Flux<Tweet>> =
      java.util.function.Function { stream ->
        stream
            .concatMap {
              logger.info("save tweet")
              tweetRepository.save(tweetEventMapper.toTweet(it))
            }
            .onErrorContinue { e, _ -> logger.log(Level.SEVERE, "an exception was thrown", e) }
      }
}
