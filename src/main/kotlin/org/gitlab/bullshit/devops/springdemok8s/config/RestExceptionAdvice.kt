package org.gitlab.bullshit.devops.springdemok8s.config

import org.springframework.http.HttpStatus
import org.springframework.http.ProblemDetail
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestControllerAdvice

@RestControllerAdvice
internal class RestExceptionAdvice {

  @ExceptionHandler
  fun handleNullPointer(e: NullPointerException): ProblemDetail {
    return ProblemDetail.forStatusAndDetail(HttpStatus.NOT_FOUND, e.message!!)
  }
}
