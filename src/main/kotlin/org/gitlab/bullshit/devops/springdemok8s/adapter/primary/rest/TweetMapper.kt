package org.gitlab.bullshit.devops.springdemok8s.adapter.primary.rest

import org.gitlab.bullshit.devops.springdemok8s.domain.Tweet
import org.mapstruct.BeanMapping
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.MappingConstants
import org.mapstruct.MappingTarget
import org.mapstruct.NullValuePropertyMappingStrategy

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
internal interface TweetMapper {

  @Mapping(target = "id", ignore = true)
  @Mapping(target = "modified_at", expression = "java(java.time.Instant.now())")
  @Mapping(target = "created_at", ignore = true)
  @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
  fun patch(@MappingTarget tweet: Tweet, map: Map<String, String>): Tweet

  @Mapping(target = "modified_at", expression = "java(java.time.Instant.now())")
  @Mapping(target = "created_at", ignore = true)
  fun toTweet(tweetResource: TweetResource): Tweet

  fun toTweetModel(tweet: Tweet): TweetResource
}
