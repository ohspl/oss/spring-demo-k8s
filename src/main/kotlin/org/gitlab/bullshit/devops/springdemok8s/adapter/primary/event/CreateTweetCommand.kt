package org.gitlab.bullshit.devops.springdemok8s.adapter.primary.event

data class CreateTweetCommand(
    val title: String = "",
    val message: String = "",
    val keywords: String = "",
    val source: String = ""
)
