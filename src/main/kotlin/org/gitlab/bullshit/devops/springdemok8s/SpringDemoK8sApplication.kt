package org.gitlab.bullshit.devops.springdemok8s

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication class SpringDemoK8sApplication

fun main(args: Array<String>) {
  runApplication<SpringDemoK8sApplication>(*args)
}
