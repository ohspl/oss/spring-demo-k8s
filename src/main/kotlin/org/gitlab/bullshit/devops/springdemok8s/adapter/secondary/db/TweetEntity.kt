package org.gitlab.bullshit.devops.springdemok8s.adapter.secondary.db

import java.time.Instant
import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Table

/** @TODO: impl database validation rules */
@Table(name = "tweet")
internal class TweetEntity(
    var created_at: Instant? = Instant.EPOCH,
    var modified_at: Instant? = Instant.EPOCH,
    var title: String = "",
    var message: String = "",
    var keywords: String = "",
    var source: String = ""
) {

  @Id var id: Long? = null
}
