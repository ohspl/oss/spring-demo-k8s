package org.gitlab.bullshit.devops.springdemok8s.showcase

import org.springframework.beans.factory.annotation.Value
import org.springframework.cloud.context.config.annotation.RefreshScope
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono

/** This controller will be needed to test the provisioned config server by operator deployment */
@RefreshScope
@RestController
@RequestMapping(path = ["/conf"], produces = [MediaType.APPLICATION_JSON_VALUE])
internal class ConfigServerPropertyController(
    @Value("\${application.showcase}") private var showcase: String = "",
) {

  @Value("\${application.showcase}") private var fieldShowcase: String = ""

  @GetMapping
  fun getShowCase(): Mono<String> {
    return Mono.just(showcase)
  }

  @GetMapping(path = ["/field"])
  fun getShowCaseField(): Mono<String> {
    return Mono.just(fieldShowcase)
  }
}
