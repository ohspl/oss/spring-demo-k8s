package org.gitlab.bullshit.devops.springdemok8s.adapter.secondary.db

import org.gitlab.bullshit.devops.springdemok8s.domain.Tweet
import org.gitlab.bullshit.devops.springdemok8s.domain.Tweets
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@Service
internal class TweetsImpl(
    private val tweetRepository: TweetRepository,
    private val tweetEntityMapper: TweetEntityMapper
) : Tweets {
  override fun findAll(): Flux<Tweet> {
    return tweetRepository.findAll().map { tweetEntityMapper.toTweet(it) }
  }

  override fun findById(id: Long): Mono<Tweet> {
    return tweetRepository.findById(id).map { tweetEntityMapper.toTweet(it) }
  }

  override fun save(tweet: Tweet): Mono<Tweet> {
    return tweetRepository.save(tweetEntityMapper.toTweetEntity(tweet)).map {
      tweetEntityMapper.toTweet(it)
    }
  }
}
