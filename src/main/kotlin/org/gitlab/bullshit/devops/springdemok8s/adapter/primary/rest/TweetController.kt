package org.gitlab.bullshit.devops.springdemok8s.adapter.primary.rest

import org.gitlab.bullshit.devops.springdemok8s.domain.Tweets
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PatchMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

/** Acts as a simple business logic controller but is just a RestController */
@RestController
// @ExposesResourceFor(Tweet::class)
@RequestMapping(path = ["tweets/"], produces = [MediaType.APPLICATION_JSON_VALUE])
internal class TweetController(private val tweets: Tweets, private val tweetMapper: TweetMapper) {

  @GetMapping(path = ["{id}/"])
  fun getTweetById(@PathVariable id: Long): Mono<ResponseEntity<TweetResource>> {
    return tweets
        .findById(id)
        .map { tweet -> ResponseEntity.ok(tweetMapper.toTweetModel(tweet)) }
        .defaultIfEmpty(ResponseEntity.notFound().build())
  }

  @GetMapping
  fun getAllTweets(): Flux<TweetResource> {
    return tweets.findAll().map { tweetMapper.toTweetModel(it) }
  }

  /** just for a formatter ignore demo */
  /** should fire a event with the right domain model * */
  // @formatter:off
  @GetMapping(path = ["stream/"], produces = [MediaType.TEXT_EVENT_STREAM_VALUE])
  fun getAllTweetsStreamed(): Flux<TweetResource> { return tweets.findAll().map { tweetMapper.toTweetModel(it) }  }
	// @formatter:on

  @PostMapping
  fun createTweet(@RequestBody tweet: TweetResource): Mono<ResponseEntity<TweetResource>> {
    return tweets.save(tweetMapper.toTweet(tweet)).map { savedTweet ->
      ResponseEntity.status(HttpStatus.CREATED).body(tweetMapper.toTweetModel(savedTweet))
    }
  }

  @PatchMapping(path = ["{id}/"])
  fun updateTweet(
      @RequestBody patchMap: Map<String, String>,
      @PathVariable id: Long
  ): Mono<ResponseEntity<TweetResource>> {
    return tweets
        .findById(id)
        .flatMap { tweets.save(tweetMapper.patch(it, patchMap)) }
        .map { tweetMapper.toTweetModel(it) }
        .map { ResponseEntity.ok(it) }
  }

  @GetMapping(path = ["tweetsnullpointer/"])
  fun throwNullPointer() {
    throw NullPointerException("ups")
  }
}
