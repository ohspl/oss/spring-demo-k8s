package org.gitlab.bullshit.devops.springdemok8s.domain

import java.time.Instant

class Tweet(
    var id: Long? = null,
    var created_at: Instant? = Instant.EPOCH,
    var modified_at: Instant? = Instant.EPOCH,
    var title: String = "",
    var message: String = "",
    var keywords: String = "",
    var source: String = ""
) {}
