package org.gitlab.bullshit.devops.springdemok8s.adapter.secondary.db

import org.gitlab.bullshit.devops.springdemok8s.domain.Tweet
import org.mapstruct.Mapper
import org.mapstruct.MappingConstants

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
internal interface TweetEntityMapper {

  fun toTweet(tweetEntity: TweetEntity): Tweet

  fun toTweetEntity(tweet: Tweet): TweetEntity
}
