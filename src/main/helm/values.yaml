## @param replicaCount Number of Spring Boot Application replicas
##
replicaCount: 1

## @param image.registry [default: REGISTRY_NAME] Spring Boot Application image registry
## @param image.repository [default: REPOSITORY_NAME/spring-demo-k8s] Spring Boot Application image repository
## @skip image.tag PostgreSQL image tag (immutable tags are recommended)
image:
  repository: ${imageRepository}
  ## Specify a imagePullPolicy
  ## Defaults to 'Always' if image tag is 'latest', else set to 'IfNotPresent'
  ## ref: https://kubernetes.io/docs/concepts/containers/images/#pre-pulled-images
  ##
  pullPolicy: IfNotPresent
  # Overrides the image tag whose default is the chart appVersion.
  tag: ${imageTag}

## @param imagePullSecrets Docker registry secret names as an array
## e.g.
## imagePullSecrets:
##   - myRegistryKeySecretName
##
imagePullSecrets: []
## @param nameOverride String to partially override common.names.fullname template (will maintain the release name)
##
nameOverride: ""
## @param fullnameOverride String to fully override common.names.fullname template
##
fullnameOverride: ""

## Service account for Spring Boot Application to use.
## ref: https://kubernetes.io/docs/tasks/configure-pod-container/configure-service-account/
##
serviceAccount:
  ## @param serviceAccount.create Enable creation of ServiceAccount for PostgreSQL pod
  ##
  create: true
  ## @param serviceAccount.name The name of the ServiceAccount to use.
  ## If not set and create is true, a name is generated using the common.names.fullname template
  ##
  name: ""
  ## @param serviceAccount.automountServiceAccountToken Allows auto mount of ServiceAccountToken on the serviceAccount created
  ## Can be set to false if pods using this serviceAccount do not need to use K8s API
  ##
  automountServiceAccountToken: true
  ## @param serviceAccount.annotations Additional custom annotations for the ServiceAccount
  ##
  annotations: {}

## Creates role for ServiceAccount
## @param rbac.create Create Role and RoleBinding
##
rbac:
  create: true

## @param podAnnotations Map of annotations to add to the pods (postgresql primary)
##
podAnnotations: {}

podSecurityContext: {}
  # fsGroup: 2000

securityContext: {}
  # capabilities:
  #   drop:
  #   - ALL
  # readOnlyRootFilesystem: true
  # runAsNonRoot: true
  # runAsUser: 1000

service:
  type: ClusterIP
  port: 8080

ingress:
  enabled: false
  className: ""
  annotations: {}
    # kubernetes.io/ingress.class: nginx
    # kubernetes.io/tls-acme: "true"
  hosts:
    - host: chart-example.local
      paths:
        - path: /
          pathType: ImplementationSpecific
  tls: []
  #  - secretName: chart-example-tls
  #    hosts:
  #      - chart-example.local

## @param resources Set container requests and limits for different resources like CPU or memory (essential for production workloads)
## Example:
## resources:
## limits:
##   cpu: 100m
##   memory: 128Mi
## requests:
##   cpu: 100m
##   memory: 128Mi
resources: {}

autoscaling:
  enabled: false
  minReplicas: 1
  maxReplicas: 3
  targetCPUUtilizationPercentage: 80
  # targetMemoryUtilizationPercentage: 80

## @param nodeSelector Node labels for Spring Boot Application pods assignment
## ref: https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/
##
nodeSelector: {}

## @param tolerations Tolerations for Spring Boot Application pods assignment
## ref: https://kubernetes.io/docs/concepts/configuration/taint-and-toleration/
##
tolerations: []

## @param affinity Affinity for Spring Boot Application pods assignment
## ref: https://kubernetes.io/docs/concepts/configuration/assign-pod-node/#affinity-and-anti-affinity
##
affinity: {}

################################################
#
#
#    S p r i n g   r e l a t e d
#
#
################################################

extraEnvVars: []
#  - name: SPRING_ACTIVE_PROFILE
#    value: kubernetes
#  - name: SPRING_DATASOURCE_PASSWORD
#    valueFrom:
#      secretKeyRef:
#        name: "test-secret"
#        key: "password"


################################################
#
#
#    H e l m    d e p e n d en c y
#
#
################################################

## @param postgresql Bitnami PostgreSQL helm chart values
## ref: https://artifacthub.io/packages/helm/bitnami/postgresql
##
postgresql:
  enabled: false
  rbac:
    create: true
  global:
    storageClass: ""
    postgresql:
      auth:
        enablePostgresUser: true
        postgresPassword: "superpass"
        username: "user"
        password: "pass"
        database: "db"
        existingSecret: ""
        secretKeys:
          adminPasswordKey: ""
          userPasswordKey: ""
          replicationPasswordKey: ""

## @param kafka Bitnami kafka helm chart values
## ref: https://artifacthub.io/packages/helm/bitnami/kafka
##
kafka:
  enabled: false
  rbac:
    create: true
  autoCreateTopicsEnable: true
  replicaCount: 1
  controller:
    replicaCount: 1
    persistence:
      enabled: false
  kraft:
    enabled: false
  zookeeper:
    enabled: true
